# Deep Neural Network(DNN)

![Demonstration](https://miro.medium.com/max/700/0*4aHRjVXRKsyUhm2b)

> A neural network is composed of input, hidden, and output layers — all of which are composed of “nodes”. Input layers take in a numerical representation of data (e.g. images with pixel specs), output layers output predictions, while hidden layers are correlated with most of the computation.

## 1. Basic Architecture

![NN](https://miro.medium.com/max/700/0*C_nYHONjxjUVgIYh)

* The neural network consists of the input layer, hidden layers and the output layer.
* Each hidden layer can have any number of neurons. Each of the outputs from the previous layer is passed to the next hidden layer.

## 2. Neurons

![Neuron](https://miro.medium.com/max/700/0*5kA2vn4UqmI4iKLM)

* The image above is a neuron from the hidden layer
* The neuron is divided into two parts. 
* The first part calculates the the sum product of weights and activations from the previous layer
* This value is passed through an activation function such as Relu or Sigmoid and this is the output of that neuron.

## 3. Activation Functions

* The **activation function** is the non linear transformation that we do over the input signal. This transformed output is then sent to the next layer of neurons as input

![Activation Functions](https://miro.medium.com/max/700/0*44z992IXd9rqyIWk.png)

* Relu is the preferred activation function for the hidden layers
* It's main advantage is that it is computationally faster than the other activations as it's function is simply max(0,x) 
* Another advantage is Relu prevents the problem of vanishing gradients. The derivative of sigmoid activation lies between 0-0.25, hence as the cnn architecture get's deeper it causes the problem of vanishing gradients.
* Sigmoid and softmax are used at the output layers for a binary and multiclass classification problem respectively.

## 4. Loss Function

* After the neural network passes its inputs all the way to its outputs, the network evaluates how good its prediction was (relative to the expected output) through something called a loss function. 
* As an example, the “Mean Squared Error” loss function is shown below.

![Loss Function](https://miro.medium.com/max/440/0*WtSWvLhcBNhQEMQo)

> Y hat represents the prediction, while Y represents the expected output. A mean is used if batches of inputs and outputs are used simultaneously (n represents sample count)

* Sum of loss function of each training example gives us the **cost function**

## 5. Back Propogation

* The goal of the network is ultimately to minimize this cost function by adjusting the weights and biases of the network. 
* In using something called “back propagation” through gradient descent (or some other optimization techniques), the network backtracks through all its layers to update the weights and biases of every node in the opposite direction of the cost function — in other words, every iteration of back propagation should result in a smaller cost function than before.

### Gradient Descent

* Gradient descent uses the first derivative (gradient) of the loss function when updating the parameters.
* The process consists in chaining the derivatives of the loss of each hidden layer from the derivatives of the loss of its upper layer, incorporating its activation function in the calculation

![Gradient Descent](https://miro.medium.com/max/700/1*qSQqO1n2-527kj_roKjbyQ.png)

* To determine the next value for the parameter, the gradient descent algorithm modifies the value of the initial weight to go in the opposite way to the gradient

![Weight Updation](https://miro.medium.com/max/700/1*VnrYE1zt2Jzm_PlpCHlq7w.png)

* The magnitude of this change is determined by the value of the gradient and by a learning rate hyperparameter that we can specify

![Weights movement](https://miro.medium.com/max/700/1*GCyRPrWsWBcPQfgEkEGMHw.png)

* The gradient descent algorithm repeats this process getting closer and closer to the minimum until the value of the parameter reaches a point beyond which the loss function cannot decrease:

![Weights movement till Local Minima](https://miro.medium.com/max/700/1*pCmHfUWN7vkyZRSGnhipzg.png)

# Conclusion

* Deep learning is ultimately an expansive field. 
* Various types of neural networks exist for different tasks (e.g. Convolutional NN for computer vision, Recurrent NN for NLP)




