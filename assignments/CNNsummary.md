# Convolutional Neural Network (CNN)

* Deep Learning algorithm which can take in an input image, assign importance (learnable weights and biases) to various aspects/objects in the image and be able to differentiate one from the other. 
* The pre-processing required in a ConvNet is much lower as compared to other classification algorithms. 
* While in primitive methods filters are hand-engineered, with enough training, ConvNets have the ability to learn these filters/characteristics.

## CNN Architecture

### 1. Input

![Input](https://miro.medium.com/max/500/1*15yDvGKV47a0nkf5qLKOOQ.png)

* This is what the input image looks like, it's defined by a set of pixels along the height, width and depth.
* The role of the ConvNet is to reduce the images into a form which is easier to process, without losing features which are critical for getting a good prediction.

### 2. Convolution Layer 

![Convolution operation](http://deeplearning.stanford.edu/wiki/images/6/6c/Convolution_schematic.gif)

* In the above demonstration, the green section resembles our 5x5x1 input image, I. 
* The element involved in carrying out the convolution operation in the first part of a Convolutional Layer is called the Kernel/Filter, K, represented in the color yellow. We have selected K as a 3x3x1 matrix.

```
Kernel/Filter, K = 
[1  0  1
0  1  0
1  0  1]
```

* The Kernel shifts 9 times because of Stride Length = 1 (Non-Strided), every time performing a matrix multiplication operation between K and the portion P of the image over which the kernel is hovering.

![Kernel movement](https://miro.medium.com/max/326/1*NsiYxt8tPDQyjyH3C08PVA@2x.png)

* When we perform the convolution operation, the size of the image shrinks. To prevent this from occuring we can **pad** the image. 

![Same Padding](https://miro.medium.com/max/395/1*nYf_cUIHFEWU1JXGwnz-Ig.gif)

* This is the formula which is used in determining the **dimension** of the activation maps:

![CNN Dimensions](https://i2.wp.com/syncedreview.com/wp-content/uploads/2017/05/13.png?resize=330%2C230&ssl=1)

* The **activation function** is the non linear transformation that we do over the input signal. This transformed output is then sent to the next layer of neurons as input

![Activation Functions](https://miro.medium.com/max/700/0*44z992IXd9rqyIWk.png)

* Relu is the preferred activation function for the hidden layers
* It's main advantage is that it is computationally faster than the other activations as it's function is simply max(0,x) 
* Another advantage is Relu prevents the problem of vanishing gradients. The derivative of sigmoid activation lies between 0-0.25, hence as the cnn architecture get's deeper it causes the problem of vanishing gradients.


### 3. Pooling Layer

* Similar to the Convolutional Layer, the Pooling layer is responsible for reducing the spatial size of the Convolved Feature. 
* This is to decrease the computational power required to process the data through dimensionality reduction. 
* It is also useful for extracting dominant features which are rotational and positional invariant, thus maintaining the process of effectively training of the model.

![Pooling operation](https://miro.medium.com/max/396/1*uoWYsCV5vBU8SHFPAPao-w.gif)

There are two types of Pooling: **Max Pooling and Average Pooling**. 
* Max Pooling returns the maximum value from the portion of the image covered by the Kernel.
* Average Pooling returns the average of all the values from the portion of the image covered by the Kernel.

![Types of pooling operation](https://miro.medium.com/max/500/1*KQIEqhxzICU7thjaQBfPBQ.png)

 ### 4. Fully Connected Layer (FC Layer)

![FC Layer](https://miro.medium.com/max/700/1*kToStLowjokojIQ7pY2ynQ.jpeg)

* In this layer, the neurons have a complete connection to all the activations from the previous layers. Their activations can hence be computed with a matrix multiplication followed by a bias offset. This is the last phase for a CNN network.
* The flattened output is fed to a feed-forward neural network and backpropagation applied to every iteration of training. 
* Over a series of epochs, the model is able to distinguish between dominating and certain low-level features in images and classify them using the Softmax Classification technique.

## Some Common pre-trained architectures

1. LeNet
2. AlexNet
3. VGGNet
4. ResNet







