# Product name
Boost new audiences for your Google and Facebook ads

# Product link
https://www.adext.ai/ai_tools/?utm_source=homepageB&utm_medium=uppermenu&utm_campaign=ai_tools

# Product Short description
The software automates all the process of campaign management and optimization, making daily adjustments per ad to super-optimize campaigns and managing budgets across multiple platforms and over several different demographic and micro demographic groups per ad.

# Product is combination of features
* Marketing Automation

# Product is provided by which company?
Adext AI
