# Product name
Driver State Monitoring

# Product link
https://www.affectiva.com/product/affectiva-automotive-ai-for-driver-monitoring-solutions/

# Product Short description
Measures, in real time, complex and nuanced emotional and cognitive states from face and voice of the driver.

* Can tell if driver is distracted by analyzing whether eyes are off the road
* Drowsiness can be detected by indicators such as yawning
* Provides solution to drowsiness such as direction to Coffee shops nearby

# Product is combination of features
* Object detection
* Person detection
* Emotion detection

# Product is provided by which company?
Affectiva
